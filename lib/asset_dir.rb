class AssetDir < Middleman::Extension
	option :dir, 'a/', 'Asset Directory'
	option :active, true, 'Use the custom asset directory'
	
	def initialize(app, options_hash={}, &block)
		super
		
		@buildid = ENV['BUILD_ID'] || Time.now.strftime('%s')
	end
	
	def dir
		d = options.dir.dup
		d.sub! /^\/+/, ''
		d.sub! /\/+$/, ''
	end
	
	def manipulate_resource_list(resources)
		if options.active
			re = %r(^#{Regexp.escape(dir)})
			
			resources.each do |r|
				r.destination_path.sub!(re, asset_dir)
			end
		end
		
		resources
	end
	
	def asset_dir
		if options.active
			"#{dir}/#{@buildid}"
		else
			dir
		end
	end
	
	helpers do
		def asset_dir
			'/'+extensions[:asset_dir].asset_dir+'/'
		end
	end
end

::Middleman::Extensions.register(:asset_dir, AssetDir)
