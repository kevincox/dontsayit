require 'pp'

class CachedCompressor
	def initialize compiler
		@cache = Hash.new do |cache, js|
			cache[js] = compiler.compress(js).chomp ";\n"
		end
	end
	
	def compress js
		@cache[js]
	end
end

# config[:jscompressor] = Uglifier.new(
# 	compress: {
# 		unsafe: true,
# 	},
# 	screw_ie8: true,
# )
config[:jscompressor] = Closure::Compiler.new(
	compilation_level: :SIMPLE,
	language_in: :ECMASCRIPT6_STRICT,
	language_out: :ECMASCRIPT5,
	warning_level: :DEFAULT,
)
config[:jscompressor] = CachedCompressor.new config[:jscompressor]

set :build_dir,  'dontsayit/'
set :css_dir,    'a/css/'
set :js_dir,     'a/js/'
set :images_dir, 'a/img/'

Dir['lib/*.rb'].each{|f| require f }

::Sass.load_paths << 'source/a/css/'

helpers do
	def c_title
		@title || current_page.data.title || ''
	end
	
	def get_content path
		path = File.absolute_path(path, File.dirname("/#{current_page.path}"))
		r    = sitemap.resources.select{|r| "/#{r.path}" == path }.first
		
		throw "No resource for #{path}" unless r
		
		r.render
	end
	
	def inline_css name, always: false, **props
		if name.is_a? Symbol
			name = "/#{config[:css_dir]}#{name}.css"
		end
		
		if development? && !always
			tag :link, rel: :stylesheet, href: name, **props
		else
			content_tag :style, **props do get_content(name).strip end
		end
	end
	def inline_js( name )
		if name.is_a? Symbol
			name = "/#{config[:js_dir]}#{name}.js"
		end
		
		if development?
			'<script src="'+escape_html(name)+'"></script>'
		else
			content_tag :script do get_content(name).strip end
		end
	end
	
	def url_dsi c
		"#{a}components/dsi-#{c}/component.html"
	end
	def import_dsi c
		tag :link, rel: :import, href: url_dsi(c)
	end
	
	def api_url_js
		if development?
			'`http://${location.hostname}:8080`'
		else
			'`https://api.dontsayit.kevincox.ca`'
		end
	end
	
	def prefetch *urls, required: false
		urls.map do |u|
			r = []
			r << tag(:link, rel: :prefetch, href: u)
			r << tag(:link, rel: :subresource, href: u) if required
			r
		end.flatten!.join
	end
	
	def _find_required page, current=nil
		if current
			direct = false
		else
			current = {}
			direct = true
		end
		
		d = page.data[:requires]
		return current unless d
		
		d.each do |k, items|
			k = k.to_sym
			current[k] ||= {}
			
			items.each do |i|
				i.match /^(\w+):(.*)$/ do |md|
					prefix = md[1]
					data   = md[2]
					
					case prefix
					when 'a'.freeze
						i = a+data
					when 'dsi'.freeze
						i = url_dsi data
					when 'http'
						throw Exception.new "Trying to link to http resource."
					when 'https'
						# Keep as is.
					else
						throw Exception.new "Unknown prefix '#{prefix}'"
					end
					
					# Don't redo done resources.
					next if current[k][i]
					
					current[k][i] = {
						direct: direct,
					}
					
					if i.start_with? '/'
						r = sitemap.resources.select{|r| "/#{r.path}" == i }.first
						_find_required r, current if r
					end
				end
			end
		end
		
		current
	end
	
	def import_required preload: false
		req = _find_required current_page
		
		html = '';
		
		req.each do |type, items|
			case type
			when :html
				items.each do |i, data|
					if data[:direct] or preload
						html << tag(:link, rel: :import, href: i)
					end
				end
			when :js
				items.each do |i, data|
					if data[:direct]
						html << '<script src="'+escape_html(i)+'"></script>'
					elsif preload
						html << prefetch(i, required: true)
					end
				end
			when :css
				items.each do |i, data|
					if data[:direct]
						html << tag(:link, rel: :stylesheet, href: i)
					elsif preload
						html << prefetch(i, required: true)
					end
				end
			else
				throw Exception.new "Unknown type #{type}"
			end
		end
		
		html
	end
	
	def a
		asset_dir
	end
end

ignores = [
	%r(^dontsayit/), # Build dir.
]
ignores.each {|p| ignore p }
set :file_watcher_ignore, ignores

page '/a/components/*.html',   layout: :component
page '/a/*/components/*.html', layout: :component
page '/a/*',                   layout: false

config[:markdown_engine] = :kramdown
# set :markdown, fenced_code_blocks: true, smartypants: true

config[:haml] = {
	remove_whitespace: true,
	attr_wrapper: '"',
	escape_html: true,
}

activate :autoprefixer, inline: true

configure :development do
	activate :livereload,
		ignore: [%r(^/a/)],
		apply_css_live: false,
		apply_js_live: false
	
	require 'rack/rewrite'
	use ::Rack::Rewrite do
		rewrite %r{^\/game/(.*)}, '/game/'
	end
	
	activate :asset_dir, dir: '/a/', active: false
end

configure :build do
	ignore_filter = lambda do |p|
		# p.start_with? '/sandbox/'
		false
	end
	
	activate :minify_javascript,
		inline: true,
		# compressor: Uglifier.new(screw_ie8: true),
		compressor: config[:jscompressor],
		ignore: ignore_filter
	
	activate :minify_css,
		inline: true,
		ignore: ignore_filter
	
	activate :asset_dir, dir: '/a/', active: true
end
