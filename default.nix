with import <nixpkgs> {}; let
	klib = import (
		builtins.fetchTarball https://github.com/kevincox/nix-lib/archive/master.tar.gz
	);
in rec {
	out = stdenv.mkDerivation  {
		name = "dontsayit";

		meta = {
			description = "Don't Say It word guessing game frontend.";
			homepage = https://dontsayit.kevincox.ca;
		};

		src = builtins.filterSource (name: type:
			((lib.hasPrefix (toString ./.git) name))) ./.;

		nativeBuildInputs = with pkgs; [
			bundler
			cacert
			git
			jre
			nodejs
		];

		buildPhase = ''
			HOME=$(pwd)/home;

			masterdate=$(git show -s --format=%cI)
			startdate=$(date -Iseconds -d "$masterdate - 2 days")
			firstcommit=$(git rev-list -n1 --before "$startdate" HEAD)

			set +e
			while read -r sha; do
				git reset --hard $sha
				git --no-pager show --pretty=short -s
				bundler --version
				bundle check || bundle install
				BUILD_ID=''${sha::4} bundle exec middleman build --verbose --no-clean
				r=$?
			done < <(git log --reverse --format=%H "$firstcommit~..")

			[ "$r" -ne 0 ] && exit "$r"
			set -e
		'';

		installPhase = ''
			mv dontsayit "$out"
		'';
	};
}
