+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-form-oneline").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-form-oneline");
}

window.DSIFormOneline = document.registerElement("dsi-form-oneline", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-form-oneline");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
				
				this.form   = r.firstChild;
				this.input  = this.form.children[0];
				
				for (var i = this.attributes.length; i--; ) {
					var a = this.attributes[i];
					this.attributeChangedCallback(a.name, null, a.value, "");
				}
			},
		},
		
		attachedCallback: {
			value() {
			},
		},
		
		detachedCallback: {
			value(){
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns){
				switch (name) {
				case "id":
				case "class":
				case "is":
				case "inset":
					break; // Ignore.
				default:
					// Pass to input element.
					this.input.setAttribute(name, n);
				}
			},
		},
		
		value: {
			get() { return this.input.value },
		},
	}),
});

}();
