+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

window.DSIJoinDialog = document.registerElement("dsi-join-dialog", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var r = this.createShadowRoot();
				r.innerHTML
					= `<dsi-form-oneline`
					+ 	` inputmode=verbatim`
					+ 	` placeholder="Game ID"`
					+ 	` title="The Game ID is a number such as '42'"`
					+ 	` pattern="\\w*"`
					+ `>`
					+	`Join Game!`
					+ `</dsi-form-oneline>`
					;
				this.form = r.firstChild;
				
				this.form.addEventListener("submit", e => {
					e.preventDefault();
					document.location = `/game/${this.form.value.toUpperCase()}`;
				});
			},
		},
	}),
});

}();
