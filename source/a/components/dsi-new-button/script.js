+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

window.DSINewButton = document.registerElement("dsi-new-button", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var r = this.createShadowRoot();
				r.innerHTML = '<dsi-button class=g><content/></dsi-button>';
				this.addEventListener("click", e => {
					DontSayIt.newGame().then(id => {
						document.location = `/game/${id}`;
					});
				});
			},
		},
	}),
});

}();
