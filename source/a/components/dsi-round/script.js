+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-round").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-round");
}

window.DSIRound = document.registerElement("dsi-round", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-round");
				this.createShadowRoot();
				this.shadowRoot.appendChild(document.importNode(t.content, true));
				
				this.root     = this.shadowRoot.querySelector("#root");
				this.timer    = this.shadowRoot.querySelector("dsi-timer");
				this.speaking = this.shadowRoot.querySelector("#speaking");
				this.guessing = this.shadowRoot.querySelector("#guessing");
				
				this._round_callback = e => {
					this.render();
				};
				
				for (var i = this.attributes.length; i--; ) {
					var a = this.attributes[i];
					this.attributeChangedCallback(a.name, null, a.value, "");
				}
				
				this.root.removeAttribute("unresolved");
				this.removeAttribute("unresolved");
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns) {
				switch (name) {
				case 'gameid':
					this._gameRegister();
					
					var gids = this.shadowRoot.querySelectorAll(".gid");
					[].forEach.call(gids, e => {
						e.gameid = this.gameid;
					});
					break;
				}
			},
		},
		
		game: {
			get() { return DontSayIt._dsi_games[this.gameid] },
		},
		
		gameid: {
			get()    { return this.getAttribute("gameid") },
			set(gid) { this.setAttribute("gameid", gid)   },
		},
		
		render: {
			value() {
				if (!this.game.speaker) return; // No round yet.
				
				this.timer.end    = this.game.roundend;
				this.timer.length = this.game.roundlen;
				
				var speaker = this.game.player == this.game.speaker;
				var curteam = this.game.player.team == this.game.speaker.team;
				
				if (curteam && !speaker) {
					this.root.classList.add("guessing");
					
					var s = this.shadowRoot.querySelectorAll(".speaker");
					[].forEach.call(s, e => {
						e.textContent = this.game.speaker.name;
					});
				} else {
					this.root.classList.remove("guessing");
				}
			},
		},
		
		_gameRegister: {
			value() {
				if (this._oldgame) {
					this._oldgame.roundchanged.remove(this._round_callback);
				}
				
				this._oldgame = this.game;
				this._round_callback();
				this.game.roundchanged.add(this._round_callback);
			},
		},
		
		_teamUpdated: {
			value(t) {
				var e;
				if (!(t in this._teams)) {
					e = document.createElement("li");
					var b = document.createElement("dsi-button");
					b.addEventListener(e => {
						this.game.joinTeam(t.id);
					});
					e.appendChild(b);
					
					this._teams[t.id] = e;
					this.teams.appendChild(e);
				} else {
					e = this._teams[t.id];
				}
				
				e.firstChild.textContent = t.name;
			},
		},
	}),
});

}();
