+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-toast-center").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-toast-center");
	s = idocument.getElementById("dsi-toast").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-toast");
}

function Allocator(start, size, prev, next) {
	start = +start || 0;
	size  = +size  || 0;
	this.start = start;
	this.end   = start+size;
	this.prev  = prev;
	this.next  = next;
}
Allocator.prototype.alloc = function allocator_alloc(size) {
	if (!this.next)
		return this.next = new Allocator(this.end, size, this);
	else if (this.next.start - this.end >= size) {
		this.next = new Allocator(this.end, size, this, this.next);
		this.next.next.prev = this.next;
		return this.next;
	} else
		return this.next.alloc(size);
};
Allocator.prototype.free = function allocator_free() {
	var p = this.prev, n = this.next;
	this.prev.next = n;
	if (n) this.next.prev = p;
};

window.DSIToastCenter = document.registerElement("dsi-toast-center", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-toast-center");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
				
				this._allocator = new Allocator();
				this._md = new WeakMap();
				
				new MutationObserver((records, mo) => {
					records.forEach(r => {
						for (var i = 0; i < r.addedNodes.length; ++i)
							this._childAdded(r.addedNodes[i]);
						for (var i = 0; i < r.removedNodes.length; ++i)
							this._childRemoved(r.removedNodes[i]);
					});
				}).observe(this, {childList: true});
			},
		},
		
		padding: {
			get() {
				if (this.hasAttribute("padding"))
					return +this.getAttribute("padding");
				else
					return 8;
			},
			set(v) {
				this.setAttribute("padding", v);
			},
		},
		
		_childAdded: {
			value(c) {
				var info = {
					space: this._allocator.alloc(c.offsetHeight + this.padding),
				};
				c.style.bottom = info.space.start + this.padding + "px";
				this._md.set(c, info);
			},
		},
		
		_childRemoved: {
			value(c) {
				var info = this._md.get(c);
				info.space.free();
			},
		},
	}),
});

window.DSIToast = document.registerElement("dsi-toast", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-toast");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
			},
		},
		
		closeIn: {
			value(t) {
				setTimeout(()=>{ this.closed = true }, t*1000);
				return this;
			},
		},
		
		closed: {
			get() { return this.hasAttribute("closed") },
			set(c) {
				if (c) this.setAttribute("closed", true);
				else   this.removeAttribute("closed");
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns){
				console.log(name, o, n, ns);
				switch (name) {
				case "closed":
					if (n) {
						// this.dispatchEvent(new CustomEvent("dsi-toast-closed", {
						// 	bubbles: true,
						// }));
						console.log("closed", this);
						this.addEventListener("transitionend", e => {
							console.log("removed", this);
							this.parentNode.removeChild(this);
						});
					}
					break; // Ignore.
				}
			},
		},
	}),
});

Object.defineProperties(DSIToast, {
	withHTML: {
		value(html) {
			var r = new DSIToast();
			r.innerHTML = html;
			return r;
		},
	},
});

}();
