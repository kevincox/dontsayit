+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-standings").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-standings");
}

window.DSIStandings = document.registerElement("dsi-standings", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-standings");
				this.createShadowRoot();
				this.shadowRoot.appendChild(document.importNode(t.content, true));
				
				this.teams = this.shadowRoot.querySelector("#teams");
				
				this._team_callback = (t) => {
					this._teamUpdated(t);
				};
				
				for (var i = this.attributes.length; i--; ) {
					var a = this.attributes[i];
					this.attributeChangedCallback(a.name, null, a.value, "");
				}
				this.removeAttribute("unresolved");
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns) {
				switch (name) {
				case 'gameid':
					this._gameRegister();
					break;
				}
			},
		},
		
		game: {
			get() { return DontSayIt._dsi_games[this.gameid] },
		},
		
		gameid: {
			get()    { return this.getAttribute("gameid") },
			set(gid) { this.setAttribute("gameid", gid)   },
		},
		
		_gameRegister: {
			value() {
				if (this._oldgame) {
					this._oldgame.teamchanged.remove(this._team_callback);
				}
				
				this._teams = {};
				this.teams.innerHTML = "";
				
				this._oldgame = this.game;
				this.game.teamchanged.add(this._team_callback);
				for (var tid in this.game.teams)
					this._teamUpdated(this.game.teams[tid]);
			},
		},
		
		_teamUpdated: {
			value(t) {
				var e = this._teams[t.id] || document.createElement("li");
				
				e.textContent = `${t.name} - ${t.score} points - ${t.players.length} members`;
				e.style.order = -t.score;
				
				this._teams[t.id] = e;
				if (!e.parentNode) this.teams.appendChild(e);
			},
		},
	}),
});

}();
