+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

window.DSILogo = document.registerElement("dsi-logo", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-logo");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
			},
		},
	}),
});

}();
