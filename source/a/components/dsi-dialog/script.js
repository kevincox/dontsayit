+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-dialog").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-dialog");
}

window.DSIDialog = document.registerElement("dsi-dialog", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-dialog");
				this.createShadowRoot();
				this.shadowRoot.appendChild(document.importNode(t.content, true));
				
				this.shadowRoot.querySelector("#fade")
					.addEventListener("click", e => {
						if (e.target != e.currentTarget) return;
						
						e.stopPropagation();
						
						this.closed = true;
					});
				
				this.removeAttribute("unresolved");
			},
		},
		
		closed: {
			get() { return this.hasAttribute("closed") },
			set(c) {
				if (c) this.setAttribute("closed", true);
				else   this.removeAttribute("closed");
			},
		},
		
		autoDetach: {
			get() { return this.hasAttribute("auto-detach") },
			set(c) {
				if (c) this.setAttribute("auto-detach", true);
				else   this.removeAttribute("auto-detach");
			},
		},
		
		attach: {
			value() {
				document.body.appendChild(this);
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns){
				switch (name) {
				case "closed":
					if (n) {
						this.dispatchEvent(new CustomEvent("dsi-dialog-closed", {
							bubbles: true,
						}));
						if (this.autoDetach) {
							this.addEventListener("transitionend", e => {
								this.parentNode.removeChild(this);
							});
						}
					}
					break; // Ignore.
				}
			},
		},
	}),
});

Object.defineProperties(DSIDialog, {
	createInfo: {
		value(short, long) {
			var e = new DSIDialog;
			
			e.autoDetach = true;
			e.innerHTML
				= `<h1>${short}</h1>`
				+ ( long?  `<p>${long}</p>` : `` )
				+ `<dsi-button>Close</dsi-button>`
				;
			
			e.lastChild.addEventListener("click", evt => e.closed = true);
			
			e.attach();
			return e;
		}
	}
});

}();
