+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

var vibrate = navigator.vibrate || naviagtor.mozVibrate || navigator.webkitVibrate;
vibrate = vibrate? vibrate.bind(navigator) : ()=>{};

window.DSIAlerts = document.registerElement("dsi-alerts", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var handled = false;
				this._wordchanged_callback = w => {
					if (!handled) {
						if (w) vibrate(200);
					}
					
					handled = false;
				};
				this._worddone_callback = info => {
					console.log(info);
					switch (info.reason) {
					case "banned":
						vibrate([100,50,100,50,100]);
						handled = true;
						break;
					}
				};
				
				this._roundchanged_callback = () => {
					// If round has started.
					if (this.game.speaker) vibrate(2000);
				};
				
				for (var i = this.attributes.length; i--; ) {
					var a = this.attributes[i];
					this.attributeChangedCallback(a.name, null, a.value, "");
				}
				
				this.removeAttribute("unresolved");
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns) {
				switch (name) {
				case "gameid":
					this._gameRegister();
					break;
				}
			},
		},
		
		game: {
			get() { return DontSayIt._dsi_games[this.gameid] },
		},
		
		gameid: {
			get()    { return this.getAttribute("gameid") },
			set(gid) { this.setAttribute("gameid", gid)   },
		},
		
		_gameRegister: {
			value() {
				if (this._oldgame) {
					this._oldgame.wordchanged .remove(this._wordchanged_callback);
					this._oldgame.worddone    .remove(this._worddone_callback);
					this._oldgame.roundchanged.remove(this._roundchanged_callback);
				}
				
				this.game.wordchanged .add(this._wordchanged_callback);
				this.game.worddone    .add(this._worddone_callback);
				this.game.roundchanged.add(this._roundchanged_callback);
				this._oldgame = this.game;
			},
		},
	}),
});

}();
