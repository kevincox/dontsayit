+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-join-team").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-join-team");
}

window.DSIJoinTeam = document.registerElement("dsi-join-team", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-join-team");
				this.createShadowRoot();
				this.shadowRoot.appendChild(document.importNode(t.content, true));
				
				this.teams    = this.shadowRoot.querySelector("#teams");
				this.teamname = this.shadowRoot.querySelector("#teamname");
				this.teamname.addEventListener("submit", e => {
					e.preventDefault();
					this.game.register(this.teamname.value);
				})
				
				this._team_callback = (t) => {
					this._teamUpdated(t);
				};
				
				for (var i = this.attributes.length; i--; ) {
					var a = this.attributes[i];
					this.attributeChangedCallback(a.name, null, a.value, "");
				}
				this.removeAttribute("unresolved");
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns) {
				switch (name) {
				case 'gameid':
					this._gameRegister();
					break;
				}
			},
		},
		
		game: {
			get() { return DontSayIt._dsi_games[this.gameid] },
		},
		
		gameid: {
			get()    { return this.getAttribute("gameid") },
			set(gid) { this.setAttribute("gameid", gid)   },
		},
		
		_gameRegister: {
			value() {
				if (this._oldgame) {
					this._oldgame.teamchanged.remove(this._team_callback);
				}
				
				this._teams = {};
				this.teams.innerHTML = "";
				
				this._oldgame = this.game;
				this.game.teamchanged.add(this._team_callback);
				for (var tid in this.game.teams)
					this._teamUpdated(this.game.teams[tid]);
			},
		},
		
		_teamUpdated: {
			value(t) {
				var e;
				if (!(t.id in this._teams)) {
					e = document.createElement("li");
					var b = document.createElement("dsi-button");
					b.addEventListener("click", e => {
						this.game.register(t.id);
					});
					e.appendChild(b);
					
					this._teams[t.id] = e;
					this.teams.appendChild(e);
				} else {
					e = this._teams[t.id];
				}
				
				e.firstChild.textContent = t.name;
			},
		},
	}),
});

}();
