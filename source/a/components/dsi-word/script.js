+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-word").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-word");
}

window.DSIWord = document.registerElement("dsi-word", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-word");
				this.createShadowRoot();
				this.shadowRoot.appendChild(document.importNode(t.content, true));
				
				this.root    = this.shadowRoot.querySelector("#root");
				this.worddiv = this.shadowRoot.querySelector("#worddiv");
				
				this.root.querySelector("#start").addEventListener("click", e => {
					this.game.startRound();
				});
				
				this.worddiv.addEventListener("click", e => {
					var b = e.target;
					if (!b.matches("dsi-button[data-action]")) return;
					
					var worde;
					if (b.closest) {
						worde = wrap(b.closest(".word[data-word]"));
					} else {
						worde = b;
						while (!worde.hasAttribute("data-word")) {
							worde = worde.parentNode || worde.host;
							if (!worde) throw new Error("Not in a word div");
						}
					}
					var word = worde.getAttribute("data-word");
					
					var c = b.textContent;
					
					switch (b.getAttribute("data-action")) {
					case "guessed":
						this.game.wordGuessed(word);
						break;
					case "unknown":
						this.game.wordUnknown(word);
						break;
					case "skip":
						this.game.wordFailedSkip(word);
						break;
					case "banned":
						this.game.wordFailedBanned(word, c);
						break;
					default:
						console.warn("Unknown action: ", b.getAttribute("data-action"));
					}
				});
				
				this.word = undefined;
				this._word_callback = () => { this.render() };
				
				for (var i = this.attributes.length; i--; ) {
					var a = this.attributes[i];
					this.attributeChangedCallback(a.name, null, a.value, "");
				}
				this.removeAttribute("unresolved");
				this.root.removeAttribute("unresolved");
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns) {
				switch (name) {
				case 'gameid':
					this._gameRegister();
					break;
				}
			},
		},
		
		game: {
			get() { return DontSayIt._dsi_games[this.gameid] },
		},
		
		gameid: {
			get()    { return this.getAttribute("gameid") },
			set(gid) { this.setAttribute("gameid", gid)   },
		},
		
		_gameRegister: {
			value() {
				if (this._oldgame) {
					this._oldgame.wordchanged.remove(this._word_callback);
				}
				
				this._oldgame = this.game;
				this.game.wordchanged.add(this._word_callback);
				
				this.render();
			},
		},
		
		render: {
			value() {
				if (this.word) {
					var oldword = this.word;
					oldword.classList.add("done");
					oldword.addEventListener("transitionend", e => {
						oldword.parentNode.removeChild(oldword);
					});
				}
				
				var word    = this.game.word;
				var speaker = (this.game.player == this.game.speaker);
				
				if (speaker) this.root.classList.add("speaker");
				else         this.root.classList.remove("speaker");
				
				if (!word) {
					this.word = undefined;
					this.root.classList.remove("word");
					return;
				}
				this.root.classList.add("word");
				
				var html = [];
				
				var worde = this.word = document.createElement("div");
				worde.className = "word";
				worde.setAttribute("data-word", word.word);
				
				html.push("<h2>");
				html.push(speaker? "Make your Team Guess" : "Mark Errors");
				html.push("</h2>");
				
				if (speaker) html.push("<dsi-button class=g data-action=guessed>");
				else         html.push("<dsi-button class=b data-action=banned>");
				html.push(word.word, '</dsi-button>');
				
				if (speaker) html.push(
					"<div class=button-row>",
						"<dsi-button data-action=skip>Skip</dsi-button>",
						"<dsi-button data-action=unknown>Unknown Word</dsi-button>",
					"</div>"
				);
				
				html.push("<h3>Banned Words</h3>");
				
				word.banned.forEach(b => {
					html.push(
						"<dsi-button class=b data-action=banned>",
						b,
						"</dsi-button>"
					);
				})
				
				worde.innerHTML = html.join("");
				this.worddiv.appendChild(worde);
				
				setTimeout(() => { worde.classList.add("active") }, 1000);
			},
		},
	}),
});

}();
