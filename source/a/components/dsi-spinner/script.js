+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-spinner").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-spinner");
}

window.DSISpinner = document.registerElement("dsi-spinner", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-spinner");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
			},
		},
	}),
});

}();
