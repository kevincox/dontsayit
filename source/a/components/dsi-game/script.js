+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-game").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-game");
}

window.DSIGame = document.registerElement("dsi-game", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-game");
				this.createShadowRoot();
				this.shadowRoot.appendChild(document.importNode(t.content, true));
				
				this.root = this.shadowRoot.querySelector("#root");
				this.root.querySelector("#menu").addEventListener("click", e => {
					this.root.leftOpen = true;
				});
				this.root.querySelector("#stats").addEventListener("click", e => {
					this.root.rightOpen = true;
				});
				
				this.root.querySelector("#change-name").addEventListener("click", e => {
					if (!this.game || !this.game.player) {
						DSIDialog.createInfo("Not Logged In", "Log in to change your name.");
						return;
					}
					
					var d = new DSIDialog;
					d.autoDetach = true;
					d.innerHTML
						= "<h1>Change Your Name</h1>"
						+ "<dsi-form-oneline placeholder='New Name' inset>"
						+	"Change"
						+ "</dsi-form-oneline>"
						;
					
					var f = d.lastChild;
					f.addEventListener("submit", e => {
						e.preventDefault();
						
						if (!this.game || !this.game.player) return;
						
						this.game.player.name = f.value;
						this.game.player.save();
						
						d.closed = true;
					});
					
					d.attach();
				});
			},
		},
		
		attachedCallback: {
			value() {
				this.removeAttribute("unresolved");
				
				this.gameid = this.getAttribute("gameid");
				if (!this.gameid) return;
				
				this.game = new DontSayIt(this.gameid);
				DontSayIt._dsi_games[this.gameid] = this.game;
				this.game.statechanged.add((o, n, g)=>{
					this.dispatchEvent(new CustomEvent("dsi-state-changed", {
						detail: {
							old: o,
							new: n,
							game: g,
						},
					}));
				});
				
				this.game.ready.then(()=>{
					this.dispatchEvent(new CustomEvent("dsi-ready", {
						detail: this.game,
					}));
				});
				
				this.shadowRoot.getElementById("title")
					.textContent = `Don't Say It — Game ${this.gameid}`;
				
				var gids = this.shadowRoot.querySelectorAll(".gid");
				[].slice.call(gids).forEach(e => {
					e.gameid = this.gameid;
				});
			},
		},
		
		detachedCallback: {
			value(){
				delete DontSayIt._dsi_games[this.gameid];
				if (this.game) {
					this.game.close();
				}
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns){
				if (!this.parentNode) return;
				
				if (name == "gameid") {
					this.detachedCallback();
					this.attachedCallback();
				}
			},
		},
	}),
});

}();
