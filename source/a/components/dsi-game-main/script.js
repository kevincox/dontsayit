+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-game-main").content.querySelector("style");
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-game-main");
}

window.DSIGameMain = document.registerElement("dsi-game-main", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				this.template = idocument.getElementById("dsi-game-main").content;
				this.root = document.importNode(
					this.template.querySelector("#root"),
					true
				);
				this._current = undefined;
				
				var r = this.createShadowRoot();
				r.appendChild(this.root);
				
				this._oldgame = undefined;
				this._gamestate_callback = (o, n, g) => {
					this.render();
				};
				
				for (var i = this.attributes.length; i--; ) {
					var a = this.attributes[i];
					this.attributeChangedCallback(a.name, null, a.value, "");
				}
				
				this.removeAttribute("unresolved");
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns) {
				switch (name) {
				case 'gameid':
					this._gameRegister();
					this.render();
					break;
				}
			},
		},
		
		game: {
			get() { return DontSayIt._dsi_games[this.gameid] },
		},
		
		gameid: {
			get()    { return this.getAttribute("gameid") },
			set(gid) { this.setAttribute("gameid", gid)   },
		},
		
		_gameRegister: {
			value() {
				if (this._oldgame) {
					this._oldgame.statechanged.remove(this._gamestate_callback);
				}
				
				this.game.statechanged.add(this._gamestate_callback);
				this._oldgame = this.game;
			},
		},
		
		render: {
			value() {
				if (!this.game) return;
				
				if (this._current)
					this.root.removeChild(this._current);
				
				var n = this.template.querySelector("#"+this.game.state);
				if (!n) {
					console.warn("Unknown game state", this.game.state);
					n = this.template.querySelector("#unknownstate");
					n.innerHTML += this.game.state;
				}
				this._current = document.importNode(wrap(n), true);
				var gids = this._current.querySelectorAll(".gameid");
				[].slice.call(gids).forEach(e=>{
					e.gameid = this.gameid;
				});
				this.root.appendChild(this._current);
			},
		},
	}),
});

}();
