+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-drawer").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-drawer");
}

window.DSIDrawer = document.registerElement("dsi-drawer", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-drawer");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
				
				this.root  = this.shadowRoot.querySelector("#root");
				this.left  = this.shadowRoot.querySelector("#left");
				this.right = this.shadowRoot.querySelector("#right");
				
				this._swipe = {};
				this.root.addEventListener("touchstart", e => {
					if (!this.classList.contains("dsi-narrow")) return;
					
					var t = e.changedTouches[0];
					var r = this.root.getBoundingClientRect();
					var prevent = true;
					if (!this.leftOpen && r.left < t.clientX && t.clientX < r.left+20) {
						this._swipe[t.identifier] = {
							x: t.clientX,
							e: this.left,
							min: 0,
							max: 1,
							tb: "translateX(calc(-100% + ",
							ta: "px))",
							end: dx => { if (dx > 50) this.leftOpen = true },
						};
					} else if (!this.rightOpen && r.right-20 < t.clientX && t.clientX < r.right) {
						this._swipe[t.identifier] = {
							x: t.clientX,
							e: this.right,
							min: -1,
							max: 0,
							tb: "translateX(calc(100% + ",
							ta: "px))",
							end: dx => { if (dx < -50) this.rightOpen = true },
						};
					} else if (this.leftOpen) {
						var ondrawer = this.querySelector(".dsi-left").contains(e.target);
						this._swipe[t.identifier] = {
							x: t.clientX,
							e: this.left,
							min: -1,
							max: 0,
							tb: "translateX(",
							ta: "px)",
							end: ondrawer?
								dx => { if (dx < -50) this.leftOpen = false }:
								dx => { if (dx <  10) this.leftOpen = false },
						};
						prevent = !ondrawer;
					} else if (this.rightOpen) {
						var ondrawer = this.querySelector(".dsi-right").contains(e.target);
						this._swipe[t.identifier] = {
							x: t.clientX,
							e: this.right,
							min: 0,
							max: 1,
							tb: "translateX(",
							ta: "px)",
							end: ondrawer?
								dx => { if (dx >  50) this.rightOpen = false }:
								dx => { if (dx > -10) this.rightOpen = false },
						};
						prevent = !ondrawer;
					} else {
						return;
					}
					if (prevent) {
						e.preventDefault();
						e.stopPropagation();
					}
				});
				this.root.addEventListener("touchmove", e => {
					var t = e.changedTouches[0];
					var ot = this._swipe[t.identifier];
					if (!ot) return;
					
					var w = ot.e.offsetWidth;
					var dx = t.clientX - ot.x;
					dx = Math.max(w*ot.min, Math.min(dx, w*ot.max));
					
					ot.e.style.transform  = ot.tb+dx+ot.ta;
					ot.e.style.transition = 'none';
				});
				this.root.addEventListener("touchend", e => {
					var t = e.changedTouches[0];
					var ot = this._swipe[t.identifier];
					if (!ot) return;
					
					var dx = t.clientX - ot.x;
					
					ot.e.style.transform  = null;
					ot.e.style.transition = null;
					
					ot.end(dx);
					
					delete this._swipe[t.identifier];
				});
				
				this.root.querySelector("#closer").addEventListener("click", e => {
					this.leftOpen = this.rightOpen = false;
				});
				
				for (var i = this.attributes.length; i--; ) {
					var a = this.attributes[i];
					this.attributeChangedCallback(a.name, null, a.value, "");
				}
				
				this.removeAttribute("unresolved");
			},
		},
		
		attachedCallback: {
			value() {
				this._mq = window.matchMedia("(max-width: 40em)");
				this._mq_listener = e => {
					if (this._mq.matches) this.classList.add("dsi-narrow");
					else                  this.classList.remove("dsi-narrow");
				};
				this._mq.addListener(this._mq_listener);
				this._mq_listener();
			},
		},
		
		detachedCallback: {
			value() {
				this._mq.removeListener(this._mq_listener);
				this._mq = this._mq_listener = null;
			},
		},
		
		leftOpen: {
			get()  { return this.hasAttribute("left-open"); },
			set(o) {
				if (o) this.setAttribute("left-open", true);
				else   this.removeAttribute("left-open");
			},
		},
		
		rightOpen: {
			get()  { return this.hasAttribute("right-open"); },
			set(o) {
				if (o) this.setAttribute("right-open", true);
				else   this.removeAttribute("right-open");
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns){
				switch (name) {
				case "left-open":
					if (n) this.removeAttribute("right-open")
					break;
				case "right-open":
					if (n) this.removeAttribute("left-open")
					break;
				}
			},
		},
	}),
});

}();
