+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

window.DSITimer = document.registerElement("dsi-timer", {
	prototype: Object.create(Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				this.style.display = "block";
				this.style.height = "1em";
				this.style.background = "hsl(0,0%,80%)";
				
				var r = this.createShadowRoot();
				
				this.bar = document.createElement("div");
				this.bar.className = "bar";
				this.bar.style.width = "100%";
				this.bar.style.height = "100%";
				this.bar.style.background = "hsl(240,100%,50%)";
				
				r.appendChild(this.bar);
			},
		},
		
		attachedCallback: {
			value(){
				this.render();
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns) {
				switch (name) {
				case "endms":
					this.setAttribute("end", n/1000);
					break;
				case "end":
					this.setAttribute("endms", n*1000);
					// fall through.
				case "length":
				case "down":
					this.render();
					break;
				case "center":
					this.bar.style.transformOrigin = n === null? "left" : "center";
					break;
				}
			},
		},
		
		render: {
			value() {
				this.bar.style.transition = `transform 0s`;
				this.bar.style.WebkitTransition = `-webkit-transform 0s`;
				
				if (!this.end || !this.length) {
					this.bar.style.WebkitTransform = // Safari
					this.bar.style.transform = `scaleX(${this._percent(0)})`;
					return;
				}
				
				this.bar.style.WebkitTransform = // Safari
				this.bar.style.transform = `scaleX(${this.percent})`;
				
				requestAnimationFrame(ts=>{
					// Rendered at initial position here.
					requestAnimationFrame(ts=>{
						// Now start the transition.
						this.bar.style.WebkitTransform = // Safari
						this.bar.style.transform = `scaleX(${this._percent(1)})`;
						this.bar.style.transition = `transform ${this.remaining}s linear`;
						this.bar.style.WebkitTransition = `-webkit-transform ${this.remaining}s linear`
					});
				});
			},
		},
		
		_percent: {
			value(r) { return this.down? 1-r : r },
		},
		
		percent: {
			get() { return this._percent(1-this.remaining/this.length) },
		},
		
		remaining: {
			get() { return Math.max(this.end - new Date()/1000, 0); },
		},
		
		length: {
			get()  { return +this.getAttribute("length") },
			set(l) { this.setAttribute("length", l) },
		},
		
		end: {
			get()  { return +this.getAttribute("end") },
			set(e) {
				if (e instanceof Date) e = +e/1000;
				
				this.setAttribute("end", e);
			},
		},
		
		endms: {
			get()  { return this.end*1000 },
			set(e) { this.setAttribute("endms", +e) },
		},
		
		down: {
			get() { return this.hasAttribute("down") },
			set(d) {
				if (d) this.setAttribute("down", "");
				else   this.removeAttribute("down");
			},
		},
		
		center: {
			get() { return this.hasAttribute("center") },
			set(d) {
				if (d) this.setAttribute("center", "");
				else   this.removeAttribute("center");
			},
		},
	})),
});

}();
