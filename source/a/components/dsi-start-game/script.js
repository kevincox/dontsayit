"use strict";

+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-start-game").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-start-game");
}

window.DSIStartGame = document.registerElement("dsi-start-game", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-start-game");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
				
				this.start = r.querySelector("#start");
				this.start.addEventListener("click", e => {
					this.game.startGame();
				});
				
				this.qr = r.querySelector("#qr");
				this.qr.setAttribute("data", this.gameurl);
			},
		},
		
		game: {
			get() { return DontSayIt._dsi_games[this.gameid] },
		},
		
		gameid: {
			get()    { return this.getAttribute("gameid") },
			set(gid) { this.setAttribute("gameid", gid) },
		},
		
		gameurl: {
			get() { return location.origin + location.pathname },
		},
	}),
});

}();
