+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-start").content.lastChild;
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-start");
}

window.DSIStart = document.registerElement("dsi-start", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-start");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
			},
		},
	}),
});

}();
