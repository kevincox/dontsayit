+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

window.DSIGameError = document.registerElement("dsi-game-error", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-game-error");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
			},
		},
		
		attachedCallback: {
			value() {
				this.render();
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns) {
				switch (name) {
				case 'gameid':
					this.render();
					break;
				}
			},
		},
		
		render: {
			value() {
				if (!this.game) return;
				
				var err = this.shadowRoot.getElementById("err");
				err.textContent = this.game.error.msg;
				
				if (this._help) {
					this._help.parentNode.removeChild(this._help);
					this._help = undefined;
				}
				if (this.game.error.e == "game-no-exist") {
					this._help = document.createElement("dsi-start");
					this.shadowRoot.appendChild(this._help);
				}
			},
		},
		
		game: {
			get() { return DontSayIt._dsi_games[this.gameid] },
		},
		
		gameid: {
			get()    { return this.getAttribute("gameid") },
			set(gid) { this.setAttribute("gameid", gid)   },
		},
	}),
});

}();
