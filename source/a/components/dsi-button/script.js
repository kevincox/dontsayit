+function(){
"use strict";

var idocument = document._currentScript.ownerDocument;

if (window.WebComponents && WebComponents.ShadowCSS) {
	var s = idocument.getElementById("dsi-button").content.lastChild;
	// s.textContent = WebComponents.ShadowCSS.shimStyle(s, "button[is=dsi-button]");
	s.textContent = WebComponents.ShadowCSS.shimStyle(s, "dsi-button");
}

window.DSIButton = document.registerElement("dsi-button", {
	prototype: Object.create(HTMLElement.prototype, {
		createdCallback: {
			value() {
				var t = idocument.getElementById("dsi-button");
				var r = this.createShadowRoot();
				r.appendChild(document.importNode(t.content, true));
				
				this.addEventListener("mousedown", e => {
					this.ripple(e.clientX, e.clientY);
				});
				
				this.addEventListener("click", e => {
					var f;
					if (this.closest) {
						f = wrap(this.closest("form"));
					} else {
						f = this;
						while (f.tagName != "FORM") {
							f = f.parentNode || f.host;
							if (!f) break; // No form parent.
						}
					}
					
					if (!f)
						return;
					if (f.matches(":invalid"))
						return console.log("Invalid, blocking submission.", this);
					
					f.dispatchEvent(new Event("submit", {
						cancelable: true,
						bubbles: true,
					}));
				});
			},
		},
		
		attachedCallback: {
			value() {
			},
		},
		
		detachedCallback: {
			value(){
			},
		},
		
		attributeChangedCallback: {
			value(name, o, n, ns){
			},
		},
		
		ripple: {
			value(x, y) {
				var r = document.createElement("div");
				r.className = "ripple";
				
				var br = this.getBoundingClientRect();
				x -= br.left;
				y -= br.top;
				
				r.style.left = `calc(${x}px - 5em)`;
				r.style.top  = `calc(${y}px - 5em)`;
				
				r.style.transition = "none";
				r.style.opacity = 0.5;
				r.style.transform = "scale(0)";
				
				this.shadowRoot.appendChild(r);
				
				requestAnimationFrame(ts1 => {
					requestAnimationFrame(ts2 => {
						r.style.transition = "transform 0.8s, opacity 0.4s 0.4s";
						r.style.opacity = 0;
						r.style.transform = "scale(1)";
						
						var transitioningprops = 2;
						r.addEventListener("transitionend", e => {
							if (--transitioningprops) return;
							r.parentNode.removeChild(r);
						});
					});
				});
			},
		},
	}),
});

}();
